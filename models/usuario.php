<?php

class Usuario {
    
    private $pdo;
    public $utilidades;

    public $title;
    public $subtitle;    
    public $page;        
    
    public $id;
    public $nombre;
    public $email;
    public $contrasena;
    public $contrasena2;

    public $errors;    

    /**
     * Constructor de modelo Usuario
     */
    public function __construct(){        

        $this->utilidades = new Utilidades();

        // Asignamos algunos datos estáticos para varias vistas
        $page = $this->utilidades->getPageName();
        $action = $this->utilidades->getPageAction();
        
        // Asignamos algunos datos estáticos                    
        if($page == 'usuario' && $action == null){
            $this->page = 'usuario';
            $this->title = 'Crear cuenta';
            $this->subtitle = 'Gratuito para siempre';
        } elseif($page == 'usuario' && $action == 'editar'){            
            $this->title = 'Bienvenido';
            $this->subtitle = 'Editar perfil';
        } elseif($page == 'usuario' && $action == 'login'){            
            $this->title = 'Iniciar sesión';
            $this->subtitle = 'Bienvenido';
        }
                
        try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }   

    /**
     * Registramos el usuario con los datos de formulario
     */
    public function registrar(Usuario $data){          

        try
		{            
        
        // Campos obligatorios
        if(empty($data->nombre) || empty($data->email) || empty($data->contrasena) || empty($data->contrasena2)){
            throw new Exception("Los campos Nombre, E-mail y contraseña son obligatorios.");
        }

        // Comprobamos si el usuario existe            
        if(self::usuarioExiste($data->email)){
            throw new Exception("El usuario ya existe.");
        }                

        // Comprobamos si las contraseñas publicadas coinciden
        if($data->contrasena !== $data->contrasena2){
            throw new Exception("Las contraseñas no coinciden.");             
        }

        $sql = "INSERT INTO usuarios (Nombre,Email,Contrasena) VALUES (?, ?, ?)";
		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->nombre,
                    $data->email, 
                    $data->contrasena                    
                )
            );
            

        $this->id = $this->pdo->lastInsertId();    
        
        return true;

		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Comprobamos si el usuario existe en la base en el momento de registrarlo
     * @param string $email El email introducido como nombre de usuario
     */
    private function usuarioExiste($usuario){

        try
		{
            
            $stm = "SELECT Email FROM usuarios WHERE email = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($usuario));

            if($stm->rowCount() > 0){
                return true;
            }                        

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}

    }

    /**
     * Comprobamos si el email esta reservado por el otro usuario. Utilizado en el momento de editar el perfil.
     */
    private function emailReservado($email,$id){
        try 
		{
            
            $stm = "SELECT Email FROM usuarios WHERE Email = ? AND Id != ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($email,$id));

            if($stm->rowCount() > 0){
                return true;
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Login principal
     * @param string $nombreUsuario El email de usuario como nombre de usuario
     * @param string $password La contraseña
     */
    public function login($nombreUsuario,$password){
        try 
		{

            $stm = "SELECT idUsuario,Email FROM usuarios WHERE Email = ? AND Contrasena = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($nombreUsuario,$password));

            if($stm->rowCount() > 0){                        
                return $stm->fetch();
            }

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Actualizar datos de usuario
     * @param object $data El objeto de usuario
     */
    public function editar($data){        

        try 
		{
            
        // Campos obligatorios
        if(empty($data->nombre) || empty($data->email) || empty($data->contrasena) || empty($data->contrasena2)){
            throw new Exception("Los campos Nombre, E-mail y contraseña son obligatorios.");
        }

        // Comprobamos si las contraseñas publicadas coinciden
        if($data->contrasena !== $data->contrasena2){
            throw new Exception("Las contraseñas no coinciden.");             
        }

        if(self::emailReservado($data->email,$data->id)){
            throw new Exception("El e-mail ya esta reservado por el otro usuario.");
        }

        $stm = "UPDATE usuarios SET 
				        Nombre              = ?, 						                                                
                        Contrasena          = ?,
                        Email               = ?                        						
				WHERE idUsuario = ?";

			$this->pdo->prepare($stm)
			     ->execute(
				    array(
                        $data->nombre,                        
                        $data->contrasena,
                        $data->email,                        
                        $data->id
					)
                );                                    
        
            return true;

        } catch (Exception $e){            
            $this->errors = $e->getMessage();            
		}
    }
    
    /**
     * Obtenemos el objeto de usuario desde la base
     * @param int $id El id recuperado desde la sesion
     */
    public function getUser($id){
        try
		{            
			$stm = $this->pdo->prepare("SELECT * FROM usuarios WHERE idUsuario = ?");
			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();        
        }
        
    }

    /**
     * Eliminar usuario
     * @param int $id El id de usuario
     */
    public function eliminar($id){
        try
		{            
			$stm = $this->pdo->prepare("DELETE FROM usuarios WHERE idUsuario = ?");
			$stm->execute(array($id));
			return true;
		} catch (Exception $e)
		{
            $this->errors = $e->getMessage();        
        }
    }

}
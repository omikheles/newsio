<?php

class Home {
    
    private $pdo;
    public $title;
    public $subtitle;    
    public $page;

    /**
     * Constructor de modelo Home
     */
    public function __construct(){        
        
        $this->utilidades = new Utilidades();

        $this->page = 'home';
        $this->title = 'Las noticias que importan';        
        $this->subtitle = 'NOTICIAS PERSONALIZADAS';

        try
		{
			// $this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }

    /**
     * Listamos los articulos desde la base
     */
    public function listar_articulos(){
        try
		{
		
			$stm = $this->pdo->prepare("SELECT * FROM articulo");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
    }



}
<?php

class Error404 {
    
    
    public $page;
    public $title;
    public $subtitle;    

    /**
     * Constructor de modelo 404
     */
    public function __construct(){        
        $this->utilidades = new Utilidades();
        $this->page = '404';
        $this->title = 'Error 404';        
        $this->subtitle = 'Esta página no está disponible.';            
    }    


}
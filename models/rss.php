<?php

class Rss {
    
    private $pdo;
    public $utilidades;

    public $id_usuario;
    public $title;
    public $subtitle;    
    public $page;

    public $rss_id;
    public $rss_url;
    public $rss_categoria;
    public $rss_pais;
    public $rss_interesante;

    public $errors;


    /**
     * Constructor de modelo Usuario
     */
    public function __construct(){

        $this->utilidades = new Utilidades();

        // Asignamos algunos datos estáticos para varias vistas
        $page = $this->utilidades->getPageName();
        $action = $this->utilidades->getPageAction();
        $this->id_usuario = isset($_SESSION['id']) ? $_SESSION['id'] : null;
        
        
        // Asignamos algunos datos estáticos                    
        if($page == 'rss' && $action == null){
            $this->page = 'rss';
            $this->title = 'Tus ultimas noticias de hoy';
            $this->subtitle = 'Bienvenido '.$this->utilidades->getNombreUsuario($this->id_usuario);
        } elseif($page == 'rss' && $action == 'administrar'){            
            $this->page = 'administrar';
            $this->title = 'Administrar';
            $this->subtitle = 'RSS';
        } elseif($page == 'rss' && $action == 'editar'){            
            $this->page = 'editar';
            $this->title = 'Editar';
            $this->subtitle = 'RSS';
        }

                
        try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }

    /**
     * Añadir un RSS nuevo     
     */
    public function nuevo_rss($data){

        try {
            
            // Campos obligatorios
            if(empty($data->rss_url) || empty($data->rss_categoria) || empty($data->rss_pais)){
                throw new Exception("Los campos URL, categoría y el país son obligatorios.");
            }

            
            // Comprobamos si el RSS es valido
            if(!@simplexml_load_file($data->rss_url)){
                throw new Exception("El RSS no es valido.");
            }

            // Comprobamos si el RSS ya existe
            if(self::rssExiste($data->rss_url,$data->id_usuario)){
                throw new Exception("El RSS ya existe.");
            }
            
            $stm = "INSERT INTO rss (url,idUsuario,idCategoria,idPais,Interesante) VALUES (?, ?, ?, ?, ?)";
            $this->pdo->prepare($stm)
                ->execute(
                    array(
                        $data->rss_url,
                        $data->id_usuario, 
                        $data->rss_categoria,
                        $data->rss_pais,
                        $data->rss_interesante
                    )
                );                
        
            return true;

        } catch (Exception $e)
		{            
            $this->errors = $e->getMessage();            
		}

    }

    /**
     * Actualizar datos de RSS
     * @param object $data El objeto de RSS
     */
    public function editar($data){        

        try 
		{
            
        // Campos obligatorios
        if(empty($data->rss_url) || empty($data->rss_categoria) || empty($data->rss_pais)){
            throw new Exception("Los campos URL, categoría y el país son obligatorios.");
        }                

        // Comprobamos si el RSS es valido
        if(!@simplexml_load_file($data->rss_url)){
            throw new Exception("El RSS no es valido.");
        }

        // Comprobamos si el RSS ya existe para el mismo usuario cuando actualizamos
        if(self::rssExisteEditar($data->rss_url,$data->rss_id)){
            throw new Exception("El RSS ya existe.");
        }

        $stm = "UPDATE rss SET 
				        url              = ?, 						                                                
                        idCategoria      = ?,
                        idPais           = ?,
                        Interesante      = ?                        
				WHERE idUsuario = ? AND idRss = ?";

			$this->pdo->prepare($stm)
			     ->execute(
				    array(
                        $data->rss_url,
                        $data->rss_categoria,
                        $data->rss_pais,
                        $data->rss_interesante,
                        $data->id_usuario,
                        $data->rss_id
					)
                );                                    
        
            return true;

        } catch (Exception $e){            
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Eliminar RSS
     */
    public function eliminar($id_usuario, $id_rss){
        try
		{            
			$stm = $this->pdo->prepare("DELETE FROM rss WHERE idUsuario = ? AND idRss = ?");
			$stm->execute(array($id_usuario,$id_rss));
			return true;
		} catch (Exception $e)
		{
            $this->errors = $e->getMessage();        
        }
    }

    /**
     * Comprobamos si el RSS ya existe
     */
    private function rssExiste($url,$id_usuario){

        try
		{
            
            $stm = "SELECT url FROM rss WHERE url = ? AND idUsuario = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($url,$id_usuario));

            if($stm->rowCount() > 0){
                return true;
            }                        

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}

    }

    /**
     * Comprobamos si el RSS ya existe
     */
    private function rssExisteEditar($url,$rss_id){

        try
		{
            
            $stm = "SELECT url FROM rss WHERE url = ? AND idRss != ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($url,$rss_id));

            if($stm->rowCount() > 0){
                return true;
            }                        

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}

    }    

     /**
     * Listar los RSS de usuario
     * @param int $id_usuario El ID de usuario
     * @param int $interesante Marcar si mostramos unicamente marcados como interesante
     * @param int $id_categoria El ID de categoria
     */
    public function listarRss($id_usuario, $interesante = null, $id_categoria = null){
        try
		{		                                                
            
            if(!empty($id_categoria)){
                $stm = $this->pdo->prepare("SELECT * FROM rss WHERE idUsuario = ? AND Interesante = 1 AND idCategoria = ? ORDER BY idRss DESC");
                $stm_array = array(
                    $id_usuario,                    
                    $id_categoria
                );
            } elseif (empty($id_categoria) && empty($interesante)) {
                $stm = $this->pdo->prepare("SELECT * FROM rss WHERE idUsuario = ? ORDER BY idRss DESC");
                $stm_array = array($id_usuario);
            } else {
                $stm = $this->pdo->prepare("SELECT * FROM rss WHERE idUsuario = ? AND Interesante = 1 ORDER BY idRss DESC");
                $stm_array = array(
                    $id_usuario
                );
            }

            $stm->execute($stm_array);
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
    }    

    /**
     * Obtener el nombre de categoria por el id
     * @param int $id El de categoria
     */
    public function getNombreCategoria($id){
        try 
		{            
            $stm = "SELECT Nombre FROM categorias WHERE idCategoria = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                
                return $stm->fetchColumn();                                
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Listar todas las categorias disponibles
     */
    public function listarCategorias()
    {        
        try
		{            
			$stm = $this->pdo->prepare("SELECT * FROM categorias ORDER BY Nombre ASC");
            $stm->execute();            
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();
        }
    }

    /**
     * Listar todos los países
     */
    public function listarPaises()
    {        
        try
		{            
			$stm = $this->pdo->prepare("SELECT * FROM paises ORDER BY Nombre ASC");
            $stm->execute();            
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();
        }
    }

    /**
     * Recuperamos el RSS desde la base por el ID
     */
    public function getRss($id_rss, $id_usuario){
        try
		{            
			$stm = $this->pdo->prepare("SELECT * FROM rss WHERE idRss = ? AND idUsuario = ?");
			$stm->execute(array($id_rss,$id_usuario));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();        
        }
        
    }

    /**
     * Recuperamos las noticias desde RSS
     */
    public function parseRss($url){        

        try {
            if(@simplexml_load_file($url)){
                $xml = simplexml_load_file($url);
                return $xml->channel;
            }
        } catch (Exception $e)
		{            
            $this->errors = $e->getMessage();
        }                    

    }

}
<?php

require_once PATH.'/models/home.php';

class HomeController
{

    private $model;

    public $utilidades;
    public $groupMsg;

    public function __construct(){
        $this->model = new Home();
        // $this->utilidades = new Utilidades();        
        $this->utilidades = $this->model->utilidades;
    }

    public function Index(){
        require_once HEADER;
        require_once PATH.'/views/home/home.php';
        require_once FOOTER;
    }
    
    /**
     * Acción de ejemplo se carga una pagina diferente basandonos en
     */
    public function Hola(){
        $this->groupMsg = "Hola grupo! Es una acción";
        require_once HEADER;
        require_once PATH.'/views/home/home.hola.php';
        require_once FOOTER;
    }
    
}
<?php

require_once PATH.'/models/rss.php';

class RssController
{

    private $model;

    public $utilidades;

    public $id_categoria;

    public function __construct(){
        
        $this->model = new Rss();        
        $this->utilidades = $this->model->utilidades;

        $this->model->id_categoria = !empty($_GET['id_categoria']) ? $this->utilidades->sanitize($_GET['id_categoria']) : null;        

        if(isset($_POST['nuevo-rss']) || isset($_POST['editar-rss'])){
            $this->model->rss_id = !empty($_POST['id']) ? $_POST['id'] : 0;
            $this->model->rss_url = !empty($_POST['rss_url']) ? $_POST['rss_url'] : null;
            $this->model->rss_categoria = !empty($_POST['rss_categoria']) ? $_POST['rss_categoria'] : null;
            $this->model->rss_pais = !empty($_POST['rss_pais']) ? $_POST['rss_pais'] : null;
            $this->model->rss_interesante = !empty($_POST['rss_interesante']) ? 1 : 0;            
        }

    }

    /**
     * Mostramos las RSS
     */
    public function Index(){
        
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }                

        $this->model->parseRss('https://www.smashingmagazine.com/feed/');
        
        require_once HEADER;
        require_once PATH.'/views/rss/rss.php';
        require_once FOOTER;
    }

    /**
     * Administrar las RSS
     */
    public function administrar(){
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }
        
        if(isset($_POST['nuevo-rss']) && $this->model->nuevo_rss($this->model)){
            header('Location: /rss/administrar#sindicado');
        }

        require_once HEADER;
        require_once PATH.'/views/rss/administrar.php';
        require_once FOOTER;

    }

    /**
     * Editar RSS especifico
     */
    public function editar(){
        if($this->utilidades->checkLogin()){

            $id = !empty($_GET['id']) ? Utilidades::sanitize($_GET['id']) : null;
            if(!$rss = $this->model->getRss($id,$_SESSION['id'])){
                header('Location: /rss/administrar');
            }            
            
        } else {
            header('Location: /usuario/login');
        }
                
        if(isset($_POST['editar-rss']) && $this->model->editar($this->model)){            
            header('Location: /rss/editar?id='.$id.'#actualizado');
        }

        require_once HEADER;
        require_once PATH.'/views/rss/editar.php';
        require_once FOOTER;

    }

    /**
     * Eliminar la cuenta de usuario
     */
    public function eliminar(){
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }

        $id_rss = !empty($_GET['id']) ? Utilidades::sanitize($_GET['id']) : null;            
        
        if($this->model->eliminar($this->model->id_usuario,$id_rss)){
            header('Location: /rss/administrar');
        }

    }
    
}
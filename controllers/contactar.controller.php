<?php

class ContactarController
{
    
    public $utilidades;    

    /**
     * Constructor de contralador Home
     */
    public function __construct(){
        
        $this->utilidades = new Utilidades();        
    }

    /**
     * Metodo por defecto
     */
    public function Index(){        

        if(!isset($_POST['contactar'])){
            die();
        }
        
        $nombre = !empty($_REQUEST['nombre']) ? $_REQUEST['nombre'] : null;
        $email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : null;
        $comment = !empty($_REQUEST['comment']) ? $_REQUEST['comment'] : null;

        $to      = 'omikheles@fp.uoc.edu';
        $subject = 'WorldNews';
        $message = $nombre.' - '.$email.' - '.$comment;
        $headers = 'From: omikheles@fp.uoc.edu' . "\r\n" .
            'Reply-To: omikheles@fp.uoc.edu' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $result = mail($to, $subject, $message, $headers);

        if($result) {
            header("HTTP/1.1 200 OK");
            echo "OK";
        } else {
            header("HTTP/1.1 404 ERROR");
            echo "ERROR";
        }        

    }   

}
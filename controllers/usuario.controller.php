<?php

require_once PATH.'/models/usuario.php';

class UsuarioController
{

    private $model;

    public $utilidades;

    public function __construct(){
        
        $this->model = new Usuario();        
        $this->utilidades = $this->model->utilidades;

        // Asignamos los valores de campos segun el submit de registro, perfil o login
        if(isset($_POST['registrar']) || isset($_POST['editar'])){
            $this->model->id = !empty($_POST['id']) ? $_POST['id'] : 0;
            $this->model->nombre = !empty($_POST['nombre']) ? $_POST['nombre'] : null;
            $this->model->email = !empty($_POST['email']) ? $_POST['email'] : null;
            $this->model->contrasena = !empty($_POST['contrasena']) ? md5($_POST['contrasena']) : null;
            $this->model->contrasena2 = !empty($_POST['contrasena2']) ? md5($_POST['contrasena2']) : null;
        } elseif(isset($_POST['login'])){
            $this->model->email = !empty($_POST['email']) ? $_POST['email'] : null;
            $this->model->contrasena = !empty($_POST['contrasena']) ? md5($_POST['contrasena']) : null;
        }

    }

    public function Index(){
        
        if($this->utilidades->checkLogin()){
            header('Location: /rss');
        }

        if(isset($_POST['registrar']) && $this->model->registrar($this->model)){
            $_SESSION['login'] = $this->model->email;
            $_SESSION['id'] = $this->model->id;
            header('Location: /usuario/#registrado');
        }
        
        require_once HEADER;
        require_once PATH.'/views/usuario/registro.php';
        require_once FOOTER;
    }

    /**
     * El formulario de entrada
     */
    public function login(){

        if($this->utilidades->checkLogin()){
            header('Location: /rss');
        }
        
        if(isset($_POST['login'])){
                        
            $username = $this->model->email;
            $password = $this->model->contrasena;
            
            if($rs = $this->model->login($username,$password)){
                                
                $_SESSION['login'] = $username;
                $_SESSION['id'] = $rs['idUsuario'];

                header('Location: /rss');
                                
            } else {
                $this->model->errors = 'Usuario no existe. Asegura que el e-mail y contraseña son correctos.';
            }
        }

        require_once HEADER;
        require_once PATH.'/views/usuario/login.php';
        require_once FOOTER;
    }
    
    /**
     * Editar datos de usuario
     */
    public function editar(){                
        if($this->utilidades->checkLogin()){            
            $user = $this->model->getUser($_SESSION['id']);                
        } else {
            header('Location: /usuario/login');
        }

        if(isset($_POST['editar']) && $this->model->editar($this->model)){
            header('Location: /usuario/editar#actualizado');
        }

        require_once HEADER;
        require_once PATH.'/views/usuario/editar.php';
        require_once FOOTER;
    }

    /**
     * Eliminar la cuenta de usuario
     */
    public function eliminar(){
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }

        $id = !empty($_GET['id']) ? Utilidades::sanitize($_GET['id']) : null;    

        if($_SESSION['id'] == $id && $this->model->eliminar($id)){
            $this->utilidades->session_end();
            header('Location: /');
        } else {            
            header('Location: /usuario/editar');
        }

    }
    
}
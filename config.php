<?php

// Iniciar session
session_start();

// Definimos las variables
define('PATH', realpath(dirname(__FILE__)));
define ( 'HEADER', PATH.'/views/header.php' );
define ( 'FOOTER', PATH.'/views/footer.php' );

// Base de datos
define('DBHOST','localhost');
define('DBNAME','newsio');
define('DBUSER','root');
define('DBPASSWORD','');


// El Core
require_once(PATH.'/core/utilidades.php');
require_once(PATH.'/core/db.php');

// Enrutador
require_once(PATH.'/core/router.php');
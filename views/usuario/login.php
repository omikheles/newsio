<section class="p-5">
        <div class="container">
            <div class="row">
                <div class="col text-center mb-3">
                    <img src="/assets/img/login.png" class="miembro__icon mb-3" alt="entrar" />
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-8">               
                    <div class="errors text-center mb-3"><?php echo $this->model->errors; ?></div>     
                    <form class="login-form" action="/usuario/login" method="POST">
                        <div class="form-row">                            
                            <div class="form-group col-md-6">                                    
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder="Contraseña">
                            </div>
                        </div>                                            
                        <div class="form-row"> 
                            <div class="form-group col text-right">                              
                                <button type="submit" name="login" class="button">Entrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10">
                    <hr class="mt-5 mb-5"/>
                </div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <h2>¿No tienes cuenta?</h2>
                    <a href="/usuario">Crear cuenta personal</a>
                </div>
            </div>

        </div>
    </section>
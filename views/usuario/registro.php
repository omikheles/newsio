<section class="p-5">
        <div class="container">
            <div class="row">
                <div class="col text-center mb-3">
                    <img src="/assets/img/crear.png" class="miembro__icon mb-3" alt="" />
                </div>
            </div>            
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10">
                    <div class="errors text-center mb-3"><?php echo $this->model->errors; ?></div>
                    <form class="register-form" action="/usuario/" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-6">                                    
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $this->model->nombre; ?>" required>
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="<?php echo $this->model->email; ?>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">                                    
                                <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder="Contraseña" required>
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="password" class="form-control" id="contrasena2" name="contrasena2" placeholder="Repitir contraseña" required>
                            </div>
                        </div>                        
                        <div class="form-row"> 
                            <div class="form-group col text-right d-md-flex justify-content-end align-items-center">
                                <p class="m-0 mr-md-3 mb-3 mb-md-0 fade-text policy">Estoy de acuerdo con la política de privacidad.</p>
                                <button type="submit" name="registrar" class="button">Crear cuenta</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
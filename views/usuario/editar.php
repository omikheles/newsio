<section class="p-5">
        <div class="container">
            <div class="row">
                <div class="col text-center mb-3">
                    <img src="/assets/img/user.png" class="miembro__icon mb-3" alt="" />
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10">
                    <div class="text-center d-none result">
                        <img src="/assets/img/checked.svg" width="100" alt="" />
                    </div>
                    <div class="errors text-center mb-3"><?php echo $this->model->errors; ?></div>
                    <form class="register-form" action="/usuario/editar" method="POST">
                        <input type="hidden" name="id" value="<?php echo !empty($user->idUsuario) ? $user->idUsuario : null; ?>">
                        <div class="form-row">
                            <div class="form-group col-md-6">                                    
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo !empty($user->Nombre) ? $user->Nombre : null; ?>" required>
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="<?php echo !empty($user->Email) ? $user->Email : null; ?>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">                                    
                                <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder="Contraseña" required>
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="password" class="form-control" id="contrasena2" name="contrasena2" placeholder="Repitir contraseña" required>
                            </div>
                        </div>                        
                        <div class="form-row"> 
                            <div class="form-group col text-right d-md-flex justify-content-end align-items-center">                                
                                <button type="submit" name="editar" class="button mr-md-4">Editar</button>
                                <a href="/usuario/eliminar?id=<?php echo $_SESSION['id']; ?>">Eliminar cuenta</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
<section class="p-5">
    <div class="container-fluid">            
        <div class="row">
            <div class="col-12 col-xl-4">
                <select class="form-control form-control-lg mb-5" name="categoria_rss">
                    <option value="">Filtrar por categoría</option>
                    <option value="0">Todas</option>                    
                    <?php foreach($this->model->listarCategorias() as $categoria): ?>
                    <option value="<?php echo $categoria->idCategoria; ?>"><?php echo $categoria->Nombre; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-12 col-xl-8 text-md-right">
                <a href="/rss/administrar" class="mb-5 d-block"><img src="/assets/img/plus.svg" alt="" width="25" class="mr-2" />Añadir el nuevo fuente de RSS</a>
            </div>
        </div>        
        <div class="row justify-content-md-center">            
            <div class="col-12">                        
                <div class="row d-md-flex">
                                             
                



            <?php if($this->model->listarRss($this->model->id_usuario,1)) : ?>

            <?php foreach($this->model->listarRss($this->model->id_usuario,1,$this->model->id_categoria) as $rss): ?>
            
            <?php //var_dump($rss); ?>

                    <div class="col-12 col-md-6 col-xl-4 mb-4">
                        <div class="card-extra-title mb-2"><a href="/rss/editar?id=<?php echo $rss->idRss; ?>">Modificar</a></div>
                        <div class="card">                        
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $this->model->parseRss($rss->url)->title; ?></h5>                            

                                    <ul class="card-list">
                                        <?php $count = 0; foreach ($this->model->parseRss($rss->url)->item as $item): if($count==5){break;} ?>
                                        <li>
                                            <a href="<?php echo $item->link; ?>" target="_blank"><?php echo $item->title; ?></a>
                                            <span class="d-block card-date mb-2"><?php echo $item->pubDate; ?></span>
                                        </li>
                                        <?php $count++; endforeach; ?>                                        
                                    </ul>

                                <div class="text-right"><a href="/rss?id_categoria=<?php echo $rss->idCategoria; ?>" class="card-category"><?php echo $this->model->getNombreCategoria($rss->idCategoria); ?></a></div>
                            </div>
                        </div>
                    </div>            

            
            <?php endforeach; ?>

            <?php else: ?>

            Aun no tienes RSS añadidas o las tienes desactivadas. <a href="/rss/administrar">Administrar los RSS</a>

            <?php endif; ?>

            </div>

            </div>
        </div>
    </div>
</section>


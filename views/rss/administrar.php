<?php
// var_dump($this->model);
?>

<section class="p-5">
        <div class="container">
            <div class="row">
                <div class="col text-center mb-3">
                    <img src="/assets/img/configurar.png" class="miembro__icon mb-3" alt="" />                    
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10">
                    <h2 class="mb-4 text-center">Añadir RSS nuevo</h2>
                    <div class="errors text-center mb-3"><?php echo $this->model->errors; ?></div>
                    <form class="" action="/rss/administrar" method="POST">
                        <div class="form-row d-md-flex align-items-center">
                            <div class="form-group col-md-12">                                    
                                <input type="text" class="form-control" id="rss_url" name="rss_url" placeholder="Introducir la URL de un RSS" value="<?php echo $this->model->rss_url; ?>">
                            </div>
                            <div class="form-group col-md-6">                                
                                <select class="form-control form-control-lg" name="rss_categoria">
                                    <option value="">Asignar categoría</option>
                                    <?php foreach($this->model->listarCategorias() as $categoria): ?>
                                    <option <?php echo ($categoria->idCategoria == $this->model->rss_categoria) ? 'selected' :  '' ?> value="<?php echo $categoria->idCategoria; ?>"><?php echo $categoria->Nombre; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <select class="form-control form-control-lg" name="rss_pais">
                                    <option value="">Asignar el país</option>
                                    <?php foreach($this->model->listarPaises() as $pais): ?>
                                    <option <?php echo ($pais->idPais == $this->model->rss_pais) ? 'selected' :  '' ?> value="<?php echo $pais->idPais; ?>"><?php echo $pais->Nombre; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col text-right d-md-flex justify-content-end align-items-center">                                
                                <div class="custom-control custom-checkbox m-0 mr-md-3 mb-3 mb-md-0">
                                    <input type="checkbox" class="custom-control-input" id="rss_interesante" name="rss_interesante" value="1" checked>
                                    <label class="custom-control-label" for="rss_interesante">Marcar como interesante y mostrar en noticias.</label>
                                </div>
                                <button type="submit" name="nuevo-rss" class="button">Añadir RSS</button>
                            </div>
                            
                        </div>                                                              
                    </form>
                </div>
            </div>

        </div>
    </section>

    <section class="pt-0 pb-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2>Vuestros RSS guardados</h2>
                </div>
            </div>

            <?php if($this->model->listarRss($this->model->id_usuario)) : ?>

            <?php foreach($this->model->listarRss($this->model->id_usuario,0,$this->model->id_categoria) as $rss): ?>
            
            <?php // var_dump($rss); ?>

            <div class="row rss-unit d-md-flex align-items-center pt-4 pb-4">
                <div class="col-12 col-md-8 mb-3 mb-md-0">
                    <div class="rss-unit__title"><?php echo $this->model->parseRss($rss->url)->title; ?></div>
                    <div class="rss-unit__url"><a href="<?php echo $rss->url; ?>"><?php echo $rss->url; ?></a></div>
                    <div class="rss-unit__meta"><?php echo $this->model->getNombreCategoria($rss->idCategoria); ?></div>                    
                </div>
                <div class="col-12 col-md-4 text-md-right rss-unit__tools">
                    <a href="/rss/editar?id=<?php echo $rss->idRss; ?>" class="mr-5">Editar RSS</a><a href="/rss/eliminar?id=<?php echo $rss->idRss; ?>">Borrar RSS</a>
                </div>
            </div>
            
            <?php endforeach; ?>

            <?php else: ?>

            Aun no tienes RSS añadidas. <a href="/rss/administrar">Administrar los RSS</a>

            <?php endif; ?>
            
        </div>
    </section>
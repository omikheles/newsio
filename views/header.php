<!doctype html>
<html lang="es">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/main.css">

    <title>Newsio - Las noticias que important.</title>
  </head>
  <body>
    
    <div class="hero<?php echo ($this->model->page == 'home') ? '' : ' hero--inner'; ?>">

        <header class="cabecera pt-2 pb-2">
            <div class="container">
                <nav class="navbar navbar-dark bg-transparent navbar-expand-lg">
                    
                    <a class="navbar-brand mr-5" href="/"><img src="/assets/img/logo.svg" width="100px" alt="Newsio" /></a>
                    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarText">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item"><a class="nav-link scrollto" href="/">Inicio</a></li>                            
                            <li class="nav-item"><a class="nav-link scrollto" href="/#nosotros">Sobre nosotros</a></li>                            
                            <li class="nav-item"><a class="nav-link scrollto" href="/#contactar">Contactar</a></li>
                        </ul>
                        <div class="extras">
                            <?php if($this->model->utilidades->checkLogin()): ?>
                            <a href="/rss" class="light mr-2"><strong>Ver mis RSS</strong></a><a href="/usuario/editar" class="mr-1">Editar perfil<img src="/assets/img/perfil.svg" class="ml-2" width="22" height="22" alt="perfil" /></a><a href="/salir">Cerrar sesión<img src="/assets/img/salir.svg" class="ml-2" width="17" height="15" alt="salir" /></a>
                            <?php else: ?>
                            <a href="/usuario/login">Iniciar sesión<img src="/assets/img/entrar.svg" class="ml-2" width="17" height="15" alt="entrar" /></a>
                            <?php endif; ?>                   
                        </div>
                        
                    </div>

                </nav>
            </div>
        </header>

        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="hero__micro-title"><?php echo $this->model->subtitle; ?></div>
                    <div class="hero__title<?php echo ($this->model->page == 'home') ? ' mb-5' : ''; ?>"><?php echo $this->model->title; ?></div>                    
                    <?php if($this->model->page == 'home'): ?>
                    <a href="/usuario" class="button button--light">Crear cuenta</a>
                    <?php elseif($this->model->page == 'rss'): ?>
                    <a href="/rss/administrar" class="button button--light d-inline-block mt-3">Administrar RSS</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
<section class="p-5" id="nosotros">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="decorate text-center mb-5">Sobre <b>Newsio</b></h1>
                    <div class="row align-items-center mb-5">
                        <div class="col-12 col-md-6 mb-5 mb-md-0">
                            <img src="/assets/img/icon.svg" width="60" height="60" class="mb-3"/>
                            <h2 class="mb-3">Newsio es vuestro punto de acceso a las noticias personalizadas.</h2>
                            <p class="mb-5">Con nuestra aplicación,  nunca perderás las noticias que te importan. Mantente al día con todas las temas que te interesan en un solo lugar - Newsio.</p>
                            <div class="text-right">
                            <a href="/usuario" class="button scrollto">Personalizar noticias</a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <img src="/assets/img/cards.jpg" class="img-full" />
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-12 mb-5">
                            <h1 class="decorate text-center mb-5">Como <b>Funciona</b></h1>                            
                        </div>
                        <div class="col-12 col-md-3 mb-5 mb-md-0">
                            <div class="paso text-center">
                                <img src="/assets/img/crear.png" class="miembro__icon mb-3" />
                                <div class="paso__title mb-2">Crear cuenta</div>                                
                            </div>
                        </div>
                        <div class="col-12 col-md-3 mb-5 mb-md-0">
                            <div class="paso text-center">
                                <img src="/assets/img/configurar.png" class="miembro__icon mb-3" />
                                <div class="paso__title mb-2">Configurar RSS</div>                                
                            </div>
                        </div>
                        <div class="col-12 col-md-3 mb-5 mb-md-0">
                            <div class="paso text-center">
                                <img src="/assets/img/nota.png" class="miembro__icon mb-3" />
                                <div class="paso__title mb-2">Seleccionar las RSS para mostrar</div>                                
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="paso text-center">
                                <img src="/assets/img/noticia.png" class="miembro__icon mb-3" />
                                <div class="paso__title mb-2">Disfrutar de las noticias personalizadas</div>                                
                            </div>
                        </div>                                                                                        
                        <div class="col-12 mt-5 text-center">                            
                            <a href="/usuario" class="button scrollto">Crear cuenta</a>                            
                        </div>
                    </div>                    

                </div>
            </div>            
        </div>        
    </section>    

    <div class="container">
        <div class="row">
            <div class="col">
                    <hr class="mt-5"/>
            </div>
        </div>
    </div>

    <section class="p-5" id="contactar">
        <div class="container">
            <div class="row mb-5">
                <div class="col text-center">
                    <h1 class="decorate text-center mb-5">Contactar con <b>Newsio</b></h1>                    
                    
                    <p>Puede contactar con nosotros a través de estos medios:</p>
                                        
                </div>                                
            </div>

            <div class="row">
                <div class="col">
                    <div class="text-center d-none result">
                        <img src="/assets/img/checked.svg" width="100" />
                    </div>
                    <form id="contact" class="contact-form" action="/contactar" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-6">                                    
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">                                    
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">  
                                <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
                            </div>                                
                        </div>
                        <div class="form-row"> 
                            <div class="form-group col text-right">
                                <button type="submit" class="button">Enviar mensaje</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
<?php

class Utilidades
{
    
/**
     * Terminar la sesión
     */
    public function session_end(){      
        session_destroy();        
    }

    /**
     * Obtener el nombre de la pagina
     */
    public function getPageName(){
        $routes = explode('/',strtok($_SERVER["REQUEST_URI"],'?'));
        $routes = !empty($routes[1]) ? $routes[1] : null;
        return strtolower($routes);
    }

    /**
     * Obtener la accion de pagina. El segundo nivel de la URL
     */
    public function getPageAction(){
        $routes = explode('/',strtok($_SERVER["REQUEST_URI"],'?'));
        $routes = !empty($routes[2]) ? $routes[2] : null;
        return strtolower($routes);
    }

    /**
     * Comprobamos si el usuario esta autorizado
     */
    public function checkLogin(){
        if (isset($_SESSION['login']) && isset($_SESSION['id'])) {
            return true;
        }
    }

    /**
     * Limpiamos cualquier valor
     */
    public function sanitize($field)
    {        
        $field = strip_tags($field);
        $field = trim($field);
        $field = stripslashes($field);
        return $field = htmlspecialchars($field);                        
    }

    /**
     * Recuperamos el nombre de usuario
     */
    public function getNombreUsuario($id){
        $pdo = Database::StartUp();
        try 
		{
            
            $stm = "SELECT Nombre FROM usuarios WHERE idUsuario = ? LIMIT 1";
            $stm = $pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                  
                return $stm->fetchColumn();                
            }

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

}

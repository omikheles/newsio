<?php

// Controlador por defecto
$controller = "home";
$routes = explode('/',strtok($_SERVER["REQUEST_URI"],'?'));

if(empty($routes[1]) )
{
    require_once PATH."/controllers/$controller.controller.php";    
    
    $controller = ucwords($controller) . 'Controller';
    $controller = new $controller;
    $controller->Index();
}
else
{
    // Obtenemos el controlador, el metodo que queremos cargar y la acción    
    $controller = strtolower($routes[1]);
    $accion = !empty($routes[2]) ? $routes[2] : 'Index';

    // Instanciamos el controlador
    if(file_exists(PATH."/controllers/$controller.controller.php")){            
        require_once PATH."/controllers/$controller.controller.php";
    } else {
        error404();
    }
    
    $controller = ucwords($controller) . 'Controller';
    $controller = new $controller;
    
    // Llamamos la accion comprobando antes si el metodo existe dentro de controlador y si es un metodo publico
    if(method_exists($controller,$accion)){
        $reflection = new ReflectionMethod($controller, $accion);
        if ($reflection->isPublic()) {
            call_user_func( array( $controller, $accion ) );
        } else {
            error404();    
        }
    } else {
        error404();
    }
}

function error404()
{                
    $host = 'http://'.$_SERVER['HTTP_HOST'].'/';    
    header('HTTP/1.1 404 Not Found');
    header("Status: 404 Not Found");
    header('Location:'.$host.'error');
    die();
}

?>